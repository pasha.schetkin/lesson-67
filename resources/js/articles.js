$(document).ready(function () {
    $('.delete').on('click', function (event) {
        const answer = confirm('Are you sure?');
        if (answer) {
            const articleId = $(this).attr('data-article-id');
            const token = $('input[type=hidden]').val();
            const article = $('#delete-article-' + articleId);
            $.ajax({
                url: `articles/${articleId}`,
                method: 'delete',
                data: {_token: token}
            })
                .done(function (response) {
                    $(article).remove();
                })
                .fail(function (response) {
                    console.log('fail response => ', response);
                });
        }
    })
});
