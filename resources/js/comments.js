$(document).ready(function () {
    $('.close-create-comment-modal').click(() => {
        $("#create-comment").trigger('reset');
    });

    $('#create-comment-btn').click(function (event) {
        event.preventDefault();
        const data = $('#create-comment').serialize();
        const articleId = $('#article_id').val();
        $.ajax({
            url: `/articles/${articleId}/comments`,
            method: "POST",
            data: data
        }).done(function (response) {
            renderData(response);
        }).fail(function (response) {
            console.log(response.responseJSON);
            renderErrors(response.responseJSON);
        });
    });

    function renderData(response) {
        const commentHtml = response.comment;
        $(commentHtml).on("click", function (e) {
            deleteComment(e);
        })
        const commentsBlock = $('.scrollit');
        $(commentsBlock).prepend(commentHtml);
        clearForm();
    }

    function clearForm() {
        $("#create-comment").trigger('reset');
        $("#exampleModalScrollable").modal('toggle');
    }

    function renderErrors(responseErrors) {
        const errors = responseErrors.errors;
        const message = responseErrors.message;
        let globalError = $(`<h5 class="modal-title text-danger" id="exampleModalScrollableTitleError">${message}</h5>`);
        const modalHeader = $('.modal-header');
        const header = $('#exampleModalScrollableTitle');
        header.hide();
        modalHeader.prepend(globalError);

        const keys = Object.keys(errors);

        keys.forEach(key => {
            let field = $(`#${key}Id`);
            field.addClass("is-invalid");
            $(`label[for="${key}Id"]`).addClass('text-danger');
            let div = $('<div>');
            let ul = $(`<ul id="${key}-errors">`);
            for (let i = 0; i < errors[key].length; i++) {
                let li = $('<li style="color: red;">');
                li.html(errors[key][i]);
                ul.append(li);
            }
            div.append(ul);
            field.parent().append(div);
        });
    }

    $('#authorId').change(function () {
        $(this).removeClass('is-invalid');
        $('#author-errors').remove();
        $(`label[for="authorId"]`).removeClass('text-danger');
        $('#exampleModalScrollableTitle').show();
        $('#exampleModalScrollableTitleError').hide();
    });

    $('#bodyId').change(function () {
        $(this).removeClass('is-invalid');
        $('#body-errors').remove();
        $(`label[for="bodyId"]`).removeClass('text-danger');
        $('#exampleModalScrollableTitle').show();
        $('#exampleModalScrollableTitleError').hide();
    });

    $(document).on("click", '.delete-comment', function (e) {
        console.log('LALALALALALALALALALALALALALALALALALALALA');
        deleteComment(e);
    });

    const deleteComment = event => {
        let element = event.currentTarget;
        const commentId = $(element).data('comment-id');
        const articleId = $('#article_id').val();
        const token = $(`#csrf-${commentId}`).val();
        // console.log(commentId);
        $.ajax({
            method: 'DELETE',
            url: `/articles/${articleId}/comments/${commentId} `,
            data: {_token: token}
        }).done(response => {
            console.log('DONE => ', response);
            $(`#comment-${commentId}`).remove();
        }).fail(response => {
            console.log('FAIL => ', response);
        });
    }
});
