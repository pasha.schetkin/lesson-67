@extends('layouts.app')

@section('content')
    <div class="col">
        <h1>Update {{$article->title}} article</h1>
    </div>
    <div class="col">
        <form method="post" action="{{route('articles.update', ['article' => $article])}}">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="article-title">Title</label>
                <input name="title" type="text" class="form-control"
                       id="article-title" value="{{$article->title}}">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Content</label>
                <textarea
                    rows="9"
                    name="content"
                    class="form-control"
                    id="exampleInputPassword1">{{$article->content}}</textarea>
            </div>

            @can('update', $article)
                <button type="submit" class="btn btn-primary">Update article</button>
            @endcan
        </form>
    </div>
    <div style="padding-top: 25px" class="col-2">
        <a href="{{route('articles.index')}}" class="btn btn-primary btn-sm
active" role="button"
           aria-pressed="true">Back</a>
    </div>
@endsection
