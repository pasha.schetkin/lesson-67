@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col">
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Attention</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" id="attention-modal-body">

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h3 class="article-title">
                {{$article->title}}
                @can('delete', $article)
                <form style="display: inline-block;" action="{{route('articles.delete', compact('article'))}}" method="post">
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-sm btn-outline-danger">Remove article</button>
                </form> |
                @endcan

                @can('edit', $article)
                    <a class="btn btn-sm btn-outline-primary" href="{{route('articles.edit', compact('article'))}}">Edit article</a>
                @endcan
            </h3>
            <blockquote class="blockquote">
                <p class="mb-2" style="text-align: justify">
                    {{$article->content}}
                </p>
                <footer class="blockquote-footer">
                    Author: {{$article->user->name}}, created in
                    <cite title="Created at article">
                        {{$article->created_at->diffForHumans()}}
                    </cite>
                </footer>
            </blockquote>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h3>
                Comments
                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal"
                        data-target="#exampleModalScrollable">
                    Add comment
                </button>
            </h3>
        </div>
    </div>
    <div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalScrollableTitle">Create new comment</h5>
                    <button type="button" class="close close-create-comment-modal" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="comment-form">
                        <form id="create-comment">
                            @csrf
                            <input type="hidden" id="article_id" value="{{$article->id}}">
                            <div class="form-group">
                                <label for="bodyId">Comment</label>
                                <textarea id="bodyId" name="body" class="form-control" rows="3"
                                          required></textarea>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary close-create-comment-modal" data-dismiss="modal">Close</button>
                    <button type="button" id="create-comment-btn" class="btn btn-primary">Comment</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col scrollit">
            @foreach($article->comments as $comment)
                <x-comment-component :comment="$comment"></x-comment-component>
            @endforeach
        </div>
    </div>
@endsection
