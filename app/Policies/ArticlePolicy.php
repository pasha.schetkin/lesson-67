<?php

namespace App\Policies;

use App\Models\Article;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class ArticlePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return Response|bool
     */
    public function viewAny(User $user)
    {
        return is_null($user) ? false : true;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     * @return Response|bool
     */
    public function create(User $user)
    {
        return is_null($user) ? false : true;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param Article $article
     * @return Response|bool
     */
    public function update(User $user, Article $article)
    {
        if ($user->is_admin) {
            return true;
        }
        return $user->id == $article->user_id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param Article $article
     * @return Response|bool
     */
    public function delete(User $user, Article $article)
    {
        if ($user->is_admin) {
            return true;
        }
        return $user->id == $article->user_id;
    }

    /**
     * @param User $user
     * @param Article $article
     * @return bool
     */
    public function edit(User $user, Article $article): bool
    {
        return $user->id == $article->user_id;
    }
}
