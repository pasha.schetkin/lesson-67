<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Comment;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class ArticlesCommentsController extends Controller
{
    /**
     * ArticlesCommentsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @param Article $article
     * @return JsonResponse
     * @throws Throwable
     */
    public function store(Request $request, Article $article)
    {
        $this->authorize('create', Comment::class);
        $request->validate([
            'body' => 'required|min:5'
        ]);

        $comment = new Comment();
        $comment->user_id = $request->user()->id;
        $comment->body = $request->input('body');
        $comment->article_id = $article->id;
        $comment->save();

        return response()->json([
            'comment' => view('components.comment-component', compact('comment'))->render()
        ], 201);
    }

    /**
     * @param Article $article
     * @param Comment $comment
     * @return JsonResponse
     * @throws AuthorizationException
     */
    function destroy(Article $article, Comment $comment): JsonResponse
    {
        $this->authorize('delete', $comment);
        $comment->delete();
        return response()->json([], 204);
    }
}
