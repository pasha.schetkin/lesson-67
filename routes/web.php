<?php

use App\Http\Controllers\ArticlesCommentsController;
use App\Http\Controllers\ArticlesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ArticlesController::class, 'index'])->name('dashboard');

Route::resource('articles', ArticlesController::class);
Route::delete('articles/{article}/destroy', [ArticlesController::class, 'delete'])->name('articles.delete');

Route::resource('articles.comments', ArticlesCommentsController::class)->only(['store', 'destroy']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
