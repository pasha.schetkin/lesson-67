<?php

namespace Tests\Feature;

use App\Models\Article;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ArticlesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var Collection|User
     */
    private $user;
    /**
     * @var Collection|Article
     */
    private $articles;

    /**
     * @var Collection|User
     */
    private $another_user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->articles = Article::factory()->count(9)->for($this->user)->create();
        $this->another_user = User::factory()->create();
    }

    /**
     * Test get success articles page (home page).
     * @group articles
     * @return void
     */
    public function test_success_get_articles()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertSeeText("Articles");
        $response->assertSeeText("Login");
        $response->assertSeeText("Register");
        $response->assertSeeText(config('app.name'));
        $response->assertViewHas('articles');

        foreach ($this->articles as $article) {
            $response->assertSeeText($article->title);
        }
    }

    /**
     * Redirect when user not login.
     * @group articles
     * @return void
     */
    public function test_redirect_if_user_not_auth()
    {
        $response = $this->get(
            route('articles.show', [
                'article' => $this->articles->first()
            ])
        );
        $response->assertRedirect(route('login'));
    }

    /**
     * Test for forbidden page.
     * @group articles
     * @return void
     */
    public function test_forbidden()
    {
        $this->actingAs($this->another_user);
        $response = $this->delete(
            route(
                'articles.destroy', ['article' => $this->articles->first()]
            )
        );
        $response->assertForbidden();
    }

    /**
     * Test for forbidden page (used base route).
     * @group articles
     * @return void
     */
    public function test_user_trying_delete_not_he_article()
    {
        $this->actingAs($this->another_user);
        $response = $this->delete(
            route(
                'articles.delete', ['article' => $this->articles->first()]
            )
        );
        $response->assertForbidden();
    }

    /**
     * An not authorized user cannot delete article.
     * @group articles
     * @return void
     */
    public function test_not_authorized_user_trying_delete_article()
    {
        $response = $this->delete(
            route(
                'articles.destroy', ['article' => $this->articles->first()]
            )
        );
        $response->assertRedirect(route('login'));
    }

    /**
     * An not authorized user cannot delete article.
     * @group articles
     * @return void
     */
    public function test_not_authorized_user_trying_delete_article_for_base_route()
    {
        $response = $this->delete(
            route(
                'articles.delete', ['article' => $this->articles->first()]
            )
        );
        $response->assertRedirect(route('login'));
    }

    /**
     * An authorized user can delete article for base route.
     * @group articles
     * @return void
     */
    public function test_authorized_user_successfully_delete_article()
    {
        $this->actingAs($this->user);
        $response = $this->delete(
            route(
                'articles.delete', ['article' => $this->articles->first()]
            )
        );
        $response->assertRedirect(route('articles.index'));
    }

    /**
     * Authority user can see article detail page.
     * @group articles
     * @return void
     */
    public function test_success_get_article_create_form()
    {
        $this->actingAs($this->user);
        $response = $this->get(route('articles.create'));
        $response->assertSuccessful();
    }

    /**
     * Not auth user can not see article create form.
     * @group articles
     * @return void
     */
    public function test_failed_get_article_create_form()
    {
        $response = $this->get(route('articles.create'));
        $response->assertRedirect(route('login'));
    }

    /**
     * Auth user can create article.
     * @group articles
     * @return void
     */
    public function test_success_create_article()
    {
        $this->actingAs($this->user);
        $data = [
            'title' => 'Test title',
            'content' => 'Test content'
        ];

        $response = $this->post(route('articles.store'), $data);
        $response->assertRedirect(route('dashboard'));
        $this->assertDatabaseHas('articles', $data);
    }

    /**
     * Not auth user can not create article.
     * @group articles
     * @return void
     */
    public function test_failed_create_article()
    {
        $data = [
            'title' => 'Test title',
            'content' => 'Test content'
        ];

        $response = $this->post(route('articles.store'), $data);
        $response->assertRedirect(route('login'));
    }

    /**
     * Not auth user can not update article.
     * @group articles
     * @return void
     */
    public function test_failed_not_auth_user_update_article()
    {
        $data = [
            'title' => 'Update title',
            'content' => 'Update content'
        ];

        $response = $this->put(route('articles.update', ['article' => $this->articles->first()]), $data);
        $response->assertRedirect(route('login'));
    }

    /**
     * An authorized user cannot change someone else's article.
     * @group articles
     * @return void
     */
    public function test_auth_user_trying_update_not_he_article()
    {
        $this->actingAs($this->another_user);
        $data = [
            'title' => 'Update title',
            'content' => 'Update content'
        ];

        $response = $this->put(route('articles.update', ['article' => $this->articles->first()]), $data);
        $response->assertForbidden();
    }

    /**
     * An authorized user cannot see edit form for someone else's article.
     * @group articles
     * @return void
     */
    public function test_auth_user_trying_get_edit_form_for_not_he_article()
    {
        $this->actingAs($this->another_user);
        $response = $this->get(route('articles.edit', ['article' => $this->articles->first()]));
        $response->assertForbidden();
    }

    /**
     * An authorized user can see edit form for he's article.
     * @group articles
     * @return void
     */
    public function test_auth_user_trying_get_edit_form_for_he_article()
    {
        $article = $this->articles->first();
        $this->actingAs($this->user);
        $response = $this->get(route('articles.edit', ['article' => $article]));
        $response->assertSuccessful();
        $response->assertSeeText($article->title);
    }

    /**
     * An authorized user can see update he's article.
     * @group articles
     * @return void
     */
    public function test_auth_user_update_he_article()
    {
        $article = $this->articles->first();
        $data = [
            'title' => 'New update title',
            'content' => 'New update content'
        ];
        $this->assertNotEquals($data['title'], $article->title);
        $this->assertNotEquals($data['content'], $article->content);
        $this->assertDatabaseMissing('articles', $data);

        $this->actingAs($this->user);
        $response = $this->put(route('articles.update', ['article' => $article]), $data);
        $response->assertRedirect(route('articles.index'));
        $this->assertDatabaseHas('articles', $data);
    }


}
